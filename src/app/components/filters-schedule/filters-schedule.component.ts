import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {FiltersService} from "../../services";
import {Specialist} from "../../models";

@Component({
  selector: 'app-filters-schedule',
  templateUrl: './filters-schedule.component.html',
  styleUrls: ['./filters-schedule.component.css']
})
export class FiltersScheduleComponent {
  @Input() specialists: Specialist[] = [];
  @Output() search = new EventEmitter();
  constructor(private filterService: FiltersService) { }

  public filterForm = new FormGroup({
    appointmentDate: new FormControl(new Date(new Date().setHours(0,0,0,0)), Validators.required),
    specialists: new FormControl([], [Validators.required, Validators.minLength(1)])
  })

  public submitSearch() {
    const day = this.filterForm.get('appointmentDate')?.value ?? '';
    const specialists = this.filterForm.get('specialists')?.value?.map((item: Specialist) => item.id) ?? [];
    this.filterService.setNewFilters([
      {name: 'day', value: (new Date(day)).toISOString()},
      {name: 'idSpecialist', value: specialists}
    ]);
    this.search.emit();
  }
}
