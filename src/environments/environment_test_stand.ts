import { Environment } from './types';

/**
 * Окружение настроек для теста.
 */
export const environment: Environment = {
  production: true,
  host: 'http://localhost:3000/',
  authority: 'http://localhost:3000/'
};
