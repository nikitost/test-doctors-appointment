FROM node:16 AS build

WORKDIR /app
# Копирование исходников
COPY . ./
# Запуск сборки
RUN npm install
RUN npm run build

# Выпуск, используя Alpine
FROM nginx:alpine AS release
WORKDIR /app
# Копировать приложение в итоговый образ из тестового
COPY --from=build /app/dist/test-doctors-appointment ./test-doctors-appointment/
COPY --from=build /app/package.json ./test-doctors-appointment/package.json
COPY --from=build /app/default.conf /etc/nginx/conf.d/default.conf
# Команды, которые выполняются при запуске контейнера
CMD ["nginx", "-g", "daemon off;"]
