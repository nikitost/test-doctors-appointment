import {Component, OnDestroy, OnInit} from '@angular/core';
import {FiltersService, ScheduleService, SpecialistsService} from "../../services";
import {Schedule, Specialist} from "../../models";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit, OnDestroy {
  public specialists: Specialist[] = [];
  public schedule: Schedule[] = [];
  private subscription = Subscription.EMPTY;
  constructor(
    private filterService: FiltersService,
    private scheduleService: ScheduleService,
    private specialistsService: SpecialistsService
  ) { }

  public ngOnInit() {
    this.subscription = this.specialistsService.getItems()
      .subscribe((response) => this.specialists = response);
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public onSearch() {
    this.scheduleService.getItems(this.filterService.lastParams)
      .subscribe((response) => this.schedule = response);
  }
}
