import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'initials'
})
export class InitialsPipe implements PipeTransform {

  transform(value: string, ...args: any[]): string {
    return value
      .split(' ')
      .map((el, i) => i === 0 ? el : el.substring(0, 1) + '.')
      .join(' ');
  }
}
