export { Environment };

/** Настройки окружения. */
type Environment = {
  /** Продуктовая сборка? */
  production: boolean;
  /** Адрес API-сервера. */
  host: string;
  /** Адрес сервиса аутентификации */
  authority: string;
};
