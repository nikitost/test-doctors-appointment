import { Component } from '@angular/core';
import {Routes} from "../../core";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  public menuItems = Routes;
}
