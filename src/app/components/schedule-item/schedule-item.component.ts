import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Slot} from "../../models";

@Component({
  selector: 'app-schedule-item',
  templateUrl: './schedule-item.component.html',
  styleUrls: ['./schedule-item.component.css']
})
export class ScheduleItemComponent {
  @Input() dateDay: Date | undefined;
  @Input() specialistName = '';
  @Input() slots: Slot[] = [];
  @Output() slotClick = new EventEmitter<Slot>();
}
