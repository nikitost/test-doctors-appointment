import {Component, OnDestroy, OnInit} from '@angular/core';
import {ScheduleService, SpecialistsService} from "../../services";
import {Schedule, Specialist} from "../../models";
import {DatePipe} from "@angular/common";
import {forkJoin, Subscription} from "rxjs";

interface PieData<T> {
  value: T;
  name: string;
}
@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css'],
  providers: [DatePipe]
})
export class AnalyticsComponent implements OnInit, OnDestroy {
  header = 'Нагрузка пациентами по дням';
  chartOptions: any;
  scheduleItems: Schedule[] = [];
  specialistItems: Specialist[] = [];
  recordsByDayOptions: any;
  recordsBySpecialistOptions: any;
  specialistLoadOptions: any;
  private subscription = Subscription.EMPTY;
  constructor(
    private scheduleService: ScheduleService,
    private specialistsService: SpecialistsService,
    private datePipe: DatePipe
  ) { }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    const schedule = this.scheduleService.getItems();//.subscribe((response) => this.scheduleItems = response);
    const specialists = this.specialistsService.getItems();
    this.subscription = forkJoin([schedule, specialists])
      .subscribe((result) => {
        this.scheduleItems = result[0];
        this.specialistItems = result[1];
        this.prepareRecordsByDayOptions();
        this.prepareRecordsBySpecialistOptions();
        this.prepareSpecialistLoadOptions();
        this.selectChart('Нагрузка пациентами по дням', this.recordsByDayOptions);
      })
  }

  private prepareRecordsByDayOptions() {
    const xDays: string[] = [];
    const yRecordsCount: number[] = [];
    this.scheduleItems.forEach((item) => {
      const day = this.datePipe.transform(item.day, 'dd.MM.YYYY')??'';
      const dayIndex = xDays.indexOf(day);
      if (dayIndex !== -1) {
        yRecordsCount[dayIndex] = yRecordsCount[dayIndex] + item.slots.filter((slot) => slot.fio !== null).length;
      } else {
        xDays.push(day);
        yRecordsCount.push(item.slots.filter((slot) => slot.fio !== null).length);
      }
    })
    this.recordsByDayOptions = {
      legend: {
        orient: 'vertical',
        left: 'left'
      },
      tooltip: {},
      xAxis: {
        data: xDays,
        silent: false,
        splitLine: {
          show: false,
        },
      },
      yAxis: {},
      series: [
        {
          name: 'Кол-во записей в этот день',
          type: 'line',
          data: yRecordsCount,
          animationDelay: (idx: any) => idx * 10,
        },
      ],
      animationEasing: 'elasticOut',
      animationDelayUpdate: (idx: any) => idx * 5,
    };
  }

  private prepareRecordsBySpecialistOptions() {
    const xSpecialists: string[] = [];
    const yRecordsCount: number[] = [];
    const yScheduledRecordsCount: number[] = [];
    this.scheduleItems.forEach((item) => {
      const specialistName = this.specialistItems.find((spec) => item.idSpecialist === spec.id)?.fullName ?? 'Неизвестный'
      const specialistIndex = xSpecialists.indexOf(specialistName);
      if (specialistIndex !== -1) {
        yRecordsCount[specialistIndex] = yRecordsCount[specialistIndex] + item.slots.length;
        yScheduledRecordsCount[specialistIndex] = yScheduledRecordsCount[specialistIndex] + item.slots.filter((slot) => slot.fio !== null).length;
      } else {
        xSpecialists.push(specialistName);
        yRecordsCount.push(item.slots.length);
        yScheduledRecordsCount.push(item.slots.filter((slot) => slot.fio !== null).length);
      }
    })
    this.recordsBySpecialistOptions = {
      legend: {
        orient: 'vertical',
        left: 'left'
      },
      tooltip: {},
      xAxis: {
        data: xSpecialists,
        silent: false,
        splitLine: {
          show: false,
        },
      },
      yAxis: {},
      series: [
        {
          name: 'Всего запланировано записей',
          type: 'bar',
          label: {
            show: true
          },
          data: yRecordsCount,
          animationDelay: (idx: any) => idx * 10,
        },
        {
          name: 'Зарезервировано записей',
          type: 'bar',
          label: {
            show: true
          },
          data: yScheduledRecordsCount,
          animationDelay: (idx: any) => idx * 10,
        }
      ],
      animationEasing: 'elasticOut',
      animationDelayUpdate: (idx: any) => idx * 5,
    };
  }

  private prepareSpecialistLoadOptions() {
    const data: PieData<number>[] = [];
    this.scheduleItems.forEach((item) => {
      const specialistName = this.specialistItems.find((spec) => item.idSpecialist === spec.id)?.fullName ?? 'Неизвестный'
      const specialistIndex = data.findIndex((searchItem) => searchItem.name === specialistName);
      if (specialistIndex !== -1) {
        data[specialistIndex].value = data[specialistIndex].value + item.slots.length;
      } else {
        data.push({value: item.slots.length, name: specialistName})
      }
    })
    this.specialistLoadOptions = {
      legend: {
        orient: 'vertical',
        left: 'left'
      },
      tooltip: {
        trigger: 'item'
      },
      series: [
        {
          name: 'Access From',
          type: 'pie',
          radius: '70%',
          data: data
        }
      ]
    };
  }

  selectChart(header: string, options: any) {
    this.header = header;
    this.chartOptions = options;
  }
}
