import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ScheduleComponent} from './components/shedule/schedule.component';
import {AnalyticsComponent} from './components/analytics/analytics.component';
import {HeaderComponent} from './components/header/header.component';
import {CalendarModule} from "primeng/calendar";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ListboxModule} from "primeng/listbox";
import {PaginatorModule} from "primeng/paginator";
import {MessageService} from "primeng/api";
import {ModalComponent} from './components/modal/modal.component';
import {HttpClientModule} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";
import {FiltersScheduleComponent} from './components/filters-schedule/filters-schedule.component';
import {TableScheduleComponent} from './components/table-schedule/table-schedule.component';
import {ScheduleItemComponent} from './components/schedule-item/schedule-item.component';
import {InitialsPipe} from './pipes/initials.pipe';
import {CardModule} from "primeng/card";
import {InputTextModule} from "primeng/inputtext";
import {ToastModule} from "primeng/toast";
import {NgxEchartsModule} from "ngx-echarts";

@NgModule({
  declarations: [
    AppComponent,
    ScheduleComponent,
    AnalyticsComponent,
    HeaderComponent,
    ModalComponent,
    FiltersScheduleComponent,
    TableScheduleComponent,
    ScheduleItemComponent,
    InitialsPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CalendarModule,
    ListboxModule,
    PaginatorModule,
    CardModule,
    InputTextModule,
    ToastModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
