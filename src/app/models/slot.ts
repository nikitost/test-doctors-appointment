export class Slot {
  constructor(
    public readonly startTime: Date,
    public readonly endTime: Date,
    public readonly fio: string | null,
  ) {
  }
}
