import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {Schedule, Slot, Specialist} from "../../models";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ModalService, ScheduleService} from "../../services";
import {MessageService} from "primeng/api";
import {Subscription} from "rxjs";

interface scheduleItem extends Schedule {
  specialistName: string
}

@Component({
  selector: 'app-table-schedule',
  templateUrl: './table-schedule.component.html',
  styleUrls: ['./table-schedule.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableScheduleComponent implements OnChanges, OnDestroy{
  @Input() scheduleItems: Schedule[] = [];
  @Input() specialists: Specialist[] = [];
  childData: scheduleItem[] = [];
  slotDay: Date | null = null;
  slotTime: Date | null = null;
  slotSpecialist = '';
  private subscription = Subscription.EMPTY;
  public patientRecordForm = new FormGroup({
    id: new FormControl<string | null> (null),
    fio: new FormControl<string | null> (null, Validators.required)
  })

  constructor(
    private modalService: ModalService,
    private scheduleService: ScheduleService,
    private messageService: MessageService
  ) {
  }

  ngOnChanges(): void {
    this.childData = this.scheduleItems.map((item) => {
      const name = this.specialists.find((spec) => spec.id === item.idSpecialist)?.fullName ?? '';
      return {
        id: item.id,
        idSpecialist: item.idSpecialist,
        day: item.day,
        specialistName: name,
        slots: item.slots
      } as scheduleItem;
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public makePatientRecord(slot: Slot, item: scheduleItem) {
    this.slotTime = slot.startTime;
    this.slotDay = item.day;
    this.slotSpecialist = item.specialistName;
    this.patientRecordForm.patchValue({
      id: item.id,
      fio: slot.fio
    })
    this.modalService.open('make-record');
  }

  public saveRecord() {
    const scheduleId = this.patientRecordForm.get('id')?.value ?? '';
    const fio = this.patientRecordForm.get('fio')?.value ?? '';
    if (this.patientRecordForm.valid) {
      const selectedSchedule = this.childData.find((item) => item.id === scheduleId)
      if (selectedSchedule !== undefined) {
        const slotIndex = selectedSchedule.slots.findIndex((slot) => slot.startTime === this.slotTime);
        if (slotIndex !== -1) {
          selectedSchedule.slots[slotIndex] = new Slot(selectedSchedule.slots[slotIndex].startTime, selectedSchedule.slots[slotIndex].endTime, fio);
        }
        this.subscription = this.scheduleService.patchFioToSchedule(scheduleId, {slots: selectedSchedule.slots})
          .subscribe({
            next: (response) => {
              this.modalService.close();
              const scheduleIndex = this.childData.findIndex((scheduleItem) => scheduleItem.id === response.id);
              if (scheduleIndex !== -1) {
                this.childData[scheduleIndex] = {...response, specialistName: this.specialists.find((spec) => spec.id === response.idSpecialist)?.fullName ?? ''};
              }
              this.messageService.add({severity: 'success', summary: 'Запись к специалисту', detail: 'Выполнена успешно'});
            },
            error: (error) => this.messageService.add({severity: 'error', summary: 'Запись к специалисту', detail: `Не выполнена, код: ${error.code}`})
          });
      }
    }
  }

  public onModalClose() {
    this.patientRecordForm.reset();
    this.modalService.close();
  }
}
