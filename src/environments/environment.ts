import { Environment } from './types';

/**
 * Дефолтное окружение настроек.
 */
export const environment: Environment = {
  production: false,
  host: 'http://localhost:3000/',
  authority: 'http://localhost:3000/'
};
