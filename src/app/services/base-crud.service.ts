import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export abstract class BaseCrudService<TCard, TCreate, TUpdate, TList> {
  protected readonly hostUrl = environment.host;
  protected abstract baseUrl: string;
  protected constructor(protected readonly httpClient: HttpClient) { }

  public getItems(params?: HttpParams): Observable<TList> {
    const url = `${this.hostUrl}${this.baseUrl}`;
    return this.httpClient.get<TList>(url, { params });
  }

  public getItem(id: string): Observable<TCard> {
    const url =`${this.hostUrl}${this.baseUrl}/${id}`;
    return this.httpClient.get<TCard>(url);
  }

  public updateItem(id: string, newItem: TUpdate): Observable<TCard> {
    const url = `${this.hostUrl}${this.baseUrl}/${id}`;
    return this.httpClient.put<TCard>(url, newItem);
  }

  public createItem(item: TCreate): Observable<TCard> {
    const url = `${this.hostUrl}${this.baseUrl}`;
    return this.httpClient.post<TCard>(url, item);
  }

  public deleteItem(id: string): Observable<unknown> {
    const url = `${this.hostUrl}${this.baseUrl}/${id}`;
    return  this.httpClient.delete<unknown>(url);
  }
}
