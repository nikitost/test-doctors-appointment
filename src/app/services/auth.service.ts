import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {BehaviorSubject, Observer} from "rxjs";
import {MessageService} from "primeng/api";
import {RegisteredUser, Roles} from "../models";
import {ModalService} from "./modal.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public userName = '';
  public userName$ = new BehaviorSubject<string>(this.userName);
  public userRole = Roles.guest as string;
  public userRole$ = new BehaviorSubject<string>(this.userRole);

  readonly authUrl = environment.authority;

  constructor(
    private httpClient: HttpClient,
    private messageService: MessageService,
    private modalService: ModalService
  ) { }

  public register(email: string, password: string, role: string) {
    const url = `${this.authUrl}register`;
    this.httpClient.post<RegisteredUser>(url, {email, password, role})
      .subscribe(this.authObserver);
  }

  public login(email: string, password: string) {
    const url = `${this.authUrl}login`;
    this.httpClient.post<RegisteredUser>(url, {email, password})
      .subscribe(this.authObserver);
  }

  private authObserver: Observer<RegisteredUser> = {
    next: (response) => {
      this.assignNameAndRole(response);
      this.modalService.close();
    },
    error: (error: HttpErrorResponse) => this.messageService.add({severity: 'error', summary: 'Аутентификация', detail: `Не выполнена\n ${error.error}`}),
    complete: () => {console.info('Аутентификация обработана')}
  }

  private assignNameAndRole(response: RegisteredUser) {
    this.userRole = response.user.role.toString();
    this.userRole$.next(this.userRole);
    this.userName = response.user.email;
    this.userName$.next(this.userName);
  }
}
