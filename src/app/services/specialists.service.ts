import {Injectable} from '@angular/core';
import {BaseCrudService} from "./base-crud.service";
import {Specialist} from "../models";

@Injectable({
  providedIn: 'root'
})
export class SpecialistsService extends BaseCrudService<Specialist, Specialist, Specialist, Specialist[]>{
  protected baseUrl = 'specialists';

}
