import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Routes as RouteItems } from './core'
import {AnalyticsComponent} from "./components/analytics/analytics.component";
import {ScheduleComponent} from "./components/shedule/schedule.component";

const routes: Routes = [
  { path: RouteItems[0].link, component: ScheduleComponent },
  { path: RouteItems[1].link, component: AnalyticsComponent },
  { path: '**', redirectTo: RouteItems[0].link}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
