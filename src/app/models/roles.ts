export enum Roles {
  admin = 'Администратор',
  user = 'Пользователь',
  guest = 'Гость'
}
