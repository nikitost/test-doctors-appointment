import {Injectable} from '@angular/core';
import {HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class FiltersService {
  private _lastParams: HttpParams;

  public get lastParams() {
    return this._lastParams;
  }
  constructor() {
    this._lastParams = new HttpParams();
  }

  public setNewFilters(filters: { name: string, value: string | string[] }[]) {
    let newParams = new HttpParams();
    filters.forEach((filterItem) => {
      if (typeof filterItem.value === 'string') {
        newParams = newParams.append(filterItem.name, filterItem.value);
      }
      else {
        filterItem.value.forEach((val) => {
          newParams = newParams.append(filterItem.name, val);
        })
      }
    })
    this._lastParams = newParams;
  }
}
