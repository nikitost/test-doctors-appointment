import {Slot} from "./slot";

export class Schedule {
  constructor(
    public readonly id: string,
    public readonly day: Date,
    public readonly idSpecialist: string,
    public readonly slots: Slot[]
  ) {
  }
}
