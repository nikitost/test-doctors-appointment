import {RouteItem} from '../models';

export const Routes: RouteItem[] = [
  {title: 'Расписание', link: 'schedule'},
  {title: 'Аналитика', link: 'analytics'}
]
