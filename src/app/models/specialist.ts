export class Specialist {
  constructor(
    public readonly id: string,
    public readonly fullName: string
  ) {
  }
}
