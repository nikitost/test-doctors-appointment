import {User} from "./user";

export class RegisteredUser{
  constructor(
    public readonly token: string,
    public readonly user: User
  ) {
  }
}
