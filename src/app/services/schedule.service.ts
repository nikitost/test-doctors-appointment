import { Injectable } from '@angular/core';
import {BaseCrudService} from "./base-crud.service";
import {Schedule} from "../models";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ScheduleService extends BaseCrudService<Schedule, Schedule, Schedule, Schedule[]> {
  protected baseUrl = 'schedule';

  public patchFioToSchedule(id: string, args: any): Observable<Schedule> {
    const url = `${this.hostUrl}${this.baseUrl}/${id}`;
    return this.httpClient.patch<Schedule>(url, args);
  }
}
